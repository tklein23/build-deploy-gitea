
### To be published


# Build gogs-gitea for Codeberg.org or local testing in VM or on dedicated testing server

## Assets and config files

Please see https://docs.gitea.io/en-us/customizing-gitea/ for details:

```Shell
etc/systemd/system/gitea.service  ## Systemd service file
etc/gitea/public/		  ## Public web files, CSS, fonts etc
etc/gitea/public/img/             ## Static images
etc/gitea/templates/              ## Templates for gitea
etc/gitea/conf/app.ini            ## Gitea config
```

## Prerequisites

You need all the tools required to build gitea (check out the gitea build instructions for details), and inkscape and imagemagick installed for image format conversion as part of the build process.

## Build KVM node for local testing

You can use the deploy Makefile described below to deploy locally (configuration and binary files are installed into /data/ and /etc/gitea), a dedicated server, or a locally running virtual machine.

If a local virtual machine is sufficient for testing (in most cases it is), you can use this script to create your VM. Take a look into the script and check out the KVM documentation for details.

```Shell
##
## createVM.sh expects virt-install configured with networking bridge interface br0 to make ${HOSTNAME_FQDN} visible in local network
##
HOSTNAME_FQDN=<hostname>
./createVM.sh ${HOSTNAME_FQDN}
```

## Build and deploy to VM or live server

The deployment will execute a number of scp+ssh calls to the target host. To avoid typing the ssh password again and again, consider using ssh-agent+ssh-add.

To build gogs-gitea and deploy to remote host, installing local templates, images, and assets:
```Shell
## locally:                 HOSTNAME_FQDN=localhost
## on VM or testing server: HOSTNAME_FQDN=<hostname>	## the hostname of your VM
## live:                    HOSTNAME_FQDN=codeberg.org
make HOSTNAME_FQDN=<hostname> deployment
```

