
HOSTNAME_FQDN := codeberg.org

export PATCHDIR := ${PWD}/local_patches
export BUILDDIR := ${PWD}/build
export GOROOT := ${BUILDDIR}/go
export GOPATH := ${BUILDDIR}/gitea
export PATH := ${GOROOT}/bin:${GOPATH}/bin:${PATH}

GOTAR = go1.11.5.$(shell uname | tr [:upper:] [:lower:])-amd64.tar.gz
ORIGIN = ssh://codeberg.org/Codeberg/gogs-gitea

IMAGE_PREFIX = etc/gitea/public/img
TARGETS = \
	${IMAGE_PREFIX}/logo.svg \
	${IMAGE_PREFIX}/logo-small.svg \
	${IMAGE_PREFIX}/logo-medium.svg \
	${IMAGE_PREFIX}/favicon.ico \
	${GOPATH}/bin/gitea

all : ${TARGETS}

${GOPATH}/bin/gitea : ${GOPATH}/src/code.gitea.io/gitea
	TAGS=bindata make -j1 -C $< generate build install

${GOPATH}/src/code.gitea.io/gitea : ${GOROOT}/bin/go
	${GOROOT}/bin/go get -d -u -v code.gitea.io/gitea
	( cd $@ && git remote rename origin github-gitea )
	( cd $@ && git remote add origin ${ORIGIN} )
	( cd $@ && git checkout release/v1.7 )
	-( cd $@ && patch -p1 < $(wildcard ${PATCHDIR}/*.diff) )

${GOROOT}/bin/go :
	mkdir -p ${GOROOT}/Downloads
	wget -c --no-verbose --directory-prefix=${GOROOT}/Downloads https://dl.google.com/go/${GOTAR}
	tar xfz ${GOROOT}/Downloads/${GOTAR} -C ${BUILDDIR}

deployment : deploy-gitea

deploy-gitea : ${GOPATH}/bin/gitea
	ssh root@${HOSTNAME_FQDN} mkdir -p /data/git/bin
	scp $< root@${HOSTNAME_FQDN}:/data/git/bin/gitea.new
	ssh root@${HOSTNAME_FQDN} mv /data/git/bin/gitea.new /data/git/bin/gitea
	-ssh root@${HOSTNAME_FQDN} systemctl stop gitea
	scp -r etc/gitea root@${HOSTNAME_FQDN}:/etc/
	ssh root@${HOSTNAME_FQDN} systemctl enable gitea
	ssh root@${HOSTNAME_FQDN} systemctl start gitea
	ssh root@${HOSTNAME_FQDN} systemctl status gitea

${IMAGE_PREFIX}/logo.svg : codeberg.svg
	cp -v $< $@
	inkscape $@ --export-plain-svg=$@ --without-gui --vacuum-defs --export-area-drawing --export-text-to-path

${IMAGE_PREFIX}/logo-medium.svg : codeberg.svg
	cp -v $< $@
	inkscape $@ --select=layer3 --verb=EditDelete --verb=FileSave --verb=FileQuit
	inkscape $@ --export-plain-svg=$@ --without-gui --vacuum-defs --export-area-drawing --export-text-to-path

${IMAGE_PREFIX}/logo-small.svg : codeberg.svg
	cp -v $< $@
	inkscape $@ --select=layer2 --verb=EditDelete --verb=FileSave --verb=FileQuit
	inkscape $@ --select=layer3 --verb=EditDelete --verb=FileSave --verb=FileQuit
	inkscape $@ --export-plain-svg=$@ --without-gui --vacuum-defs --export-area-drawing --export-text-to-path

${IMAGE_PREFIX}/favicon.ico : ${IMAGE_PREFIX}/logo-small.svg
	convert -density 384 $< -define icon:auto-resize $@

clean :
	${MAKE} -C ${GOPATH}/src/code.gitea.io/gitea clean
	${RM} ${TARGETS}

realclean :
	rm -rf ${BUILDDIR}

